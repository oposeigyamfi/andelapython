import pyodbc

selectcommand = ('select * from Picture where text =?')
insertcommand = (
    "insert into Picture (Text, BgColor,ForeColor) VALUES (?,?,?)")

constring = 'Driver=SQL Server;Server=.\\sqlexpress;Database=andela;Trusted_Connection=yes;'


def getFromDb(tx):

    connection = pyodbc.connect(constring)
    cursor = connection.cursor()

    values = [tx]
    cursor.execute(selectcommand, values)

    results = cursor.fetchone()

    connection.close()

    return results


def saveToDb(tx, bg, fg):

    connection = pyodbc.connect(constring)
    cursor = connection.cursor()

    values = [tx, bg, fg]
    cursor.execute(insertcommand, values)

    connection.commit()
    connection.close()
