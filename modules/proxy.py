import logging
import dbproxy
import requests

from PIL import Image
from io import BytesIO


class proxy(object):
    uri = None
    path = None

    def __init__(self):
        proxy.uri = 'https://dummyimage.com/'
        proxy.path = "E:\\andela-python\\imgs\\"

    def request(self, foreground, bgcolor, text):
        qstring = foreground + '/' + bgcolor + '/' + '?text=' + text
        response = requests.get(proxy.uri + qstring)
        return response

    def apiresponse(self, fg, bg, tx):
        try:
            found = dbproxy.getFromDb(tx)

            if(found != None):
                return self.getImageFromDsk(tx)

            response = self.request(fg, bg, tx)

            if(response.status_code == 200):
                image = Image.open(BytesIO(response.content))
                image.save(proxy.path + tx + '.png')
                dbproxy.saveToDb(tx, bg, fg)
                return image

            else:
                raise Exception("sorry request could not be completed")

        except Exception as ex:
            logging.error(ex)

    def getImageFromDsk(self, tx):
        file = open(proxy.path + tx + '.png', "r")
        return file